import streamlit as st
import pandas as pd
from datetime import date
from datetime import datetime
from zipfile import ZipFile
from urllib.request import urlopen
import urllib.parse
import os
import io
import requests

import geopandas
import json

from bokeh.io import output_notebook, show, output_file
from bokeh.plotting import figure
from bokeh.models import GeoJSONDataSource, LinearColorMapper, ColorBar, NumeralTickFormatter
from bokeh.palettes import brewer
from bokeh.models import Slider, HoverTool, Select, CustomJS

from bokeh.layouts import layout, column, row
from bokeh.models.widgets import DateRangeSlider

#from bokeh.io import curdoc

###gobal dataframe for Bezirke.csv data
bezirkeFrame = pd.DataFrame()
###global geosource
geosource = GeoJSONDataSource()
# Bokeh uses geojson formatting, representing geographical features, with json
# Read the geojson map file for Bezirke into a GeoDataframe object
gdf = geopandas.read_file('maps/bezirke.json')

###gobal dataframe for Bezirke.csv data
departamentos_co = pd.DataFrame()
###global geosource
geosource_co = GeoJSONDataSource()
# Bokeh uses geojson formatting, representing geographical features, with json
# Read the geojson map file for Bezirke into a GeoDataframe object
gdf_co = geopandas.read_file('maps/colombia-departamentos.json')




















###data
target_dir = 'data/'
d_list_co = list()
p_list_co = list()

def load_raw_data():
    BASE_URL = ('https://github.com/statistikat/coronaDAT/raw/master/archive/')
    range = pd.date_range(start='3/1/2020', end='13/7/2020')
    range_filenames = []
    target_dir = 'data/'
    for val in range:
        date_str = "{0:0=2d}".format(val.year) + "{0:0=2d}".format(val.month) + "{0:0=2d}".format(val.day)
        filename = date_str + '_orig_csv.zip'
        path = date_str + "/data/" + filename
        try:
            url = urlopen(BASE_URL + path)
        except urllib.error.URLError as e:
            # st.write(e.reason)
            continue

        zip_location = target_dir + filename
        output = open(zip_location, 'wb')  # note the flag:  "wb"
        output.write(url.read())
        output.close()

        try:
            os.mkdir(target_dir + date_str)
        except OSError:
            pass

        with ZipFile(zip_location, 'r') as zip:
            zip.extractall(path=(target_dir + date_str), members=['Bezirke.csv'])
            zip.close()
            os.remove(zip_location)
            range_filenames.append(target_dir + date_str + '/' + 'Bezirke.csv')

    return (range_filenames)

#@st.cache
def load_bezirke():
    global bezirkeFrame
    bezirkeFrame.truncate()

    li = []
    counter = 0

    for root, dirs, files in os.walk('data/'):
        for dir in dirs:
            file = 'data/' + dir + '/Bezirke.csv'
            if os.path.exists(file):
                data = pd.read_csv(file, sep=";", decimal=",", thousands=".",
                                   header=0, dtype={'GKZ': str})
                data['TimeLabel'] = dir[0:4] + '-' + dir[4:6] + '-' + dir[6:8]
                data['TimeLabel2'] = int(dir)
                # calculate daily increase
                if counter > 0:
                    infectedDelta = data['Anzahl'] - infectedCache
                    data['Delta'] = infectedDelta

                li.append(data)
                infectedCache = data['Anzahl']
                counter = counter + 1

    bezirkeFrame = pd.concat(li, axis=0, ignore_index=True)
    bezirkeFrame = bezirkeFrame[bezirkeFrame['GKZ'].notna()]
    bezirkeFrame.rename(columns={'Anzahl': 'Infected'}, inplace=True)
    bezirkeFrame.rename(columns={'GKZ': 'iso'}, inplace=True)

    return

def get_slider_range_dates():
    datess = pd.unique(bezirkeFrame['TimeLabel'])
    st.write(datess, len(datess))
    #date_time_str = '18/09/19 01:55:19'
    date_time_str_1 = datess[0]#'18/09/19'
    date_time_str_2 = datess[-1]
    date_time_str_3 = datess[-2]

    date_time_obj_1 = datetime.strptime(date_time_str_1, '%Y-%m-%d').date()

    date_time_obj_2 = datetime.strptime(date_time_str_2, '%Y-%m-%d').date()

    date_time_obj_3 = datetime.strptime(date_time_str_3, '%Y-%m-%d').date()

    st.write(date_time_obj_1.date(), date_time_obj_2.date(), date_time_obj_3.date())

    st.write(datess[0])
    st.write(date(2020, 6, 1))

    date_range_slider = DateRangeSlider(
        title="Select a Date Range: ",
        start=date_time_obj_1,
        end=date_time_obj_2,
        value=(
            date_time_obj_2, 
            date_time_obj_3),
        step=1)

    # Make a slider object: slider
    slider = Slider(title = 'Year',start = 2009, end = 2018, step = 1, value = 2018)
    #slider.on_change('value', update_plot)

    return date_range_slider

def get_today_date():
    datess = pd.unique(bezirkeFrame['TimeLabel'])
    #st.write(datess, len(datess))
    date_time_str_1 = datess[0]
    date_time_str_2 = datess[-1]
    date_time_obj_1 = datetime.strptime(date_time_str_1, '%Y-%m-%d').date()
    date_time_obj_2 = datetime.strptime(date_time_str_2, '%Y-%m-%d').date()
    today = str(date_time_obj_2)
    return today

def get_slider_date():

    datess = pd.unique(bezirkeFrame['TimeLabel'])
    st.write(datess, len(datess))
    date_time_str_1 = datess[0]
    date_time_str_2 = datess[-1]
    date_time_obj_1 = datetime.strptime(date_time_str_1, '%Y-%m-%d').date()
    date_time_obj_2 = datetime.strptime(date_time_str_2, '%Y-%m-%d').date()

    # Make a slider object: slider
    date_slider = Slider(
        title="Select a Date: ",
        start=date_time_obj_1,
        end=date_time_obj_2,
        value=date_time_obj_2,
        step=1)

    # NOT POSSIBLE TO WRITE A CALLBACK CALLING A PYTHON FUNCTION
    # https://groups.google.com/a/continuum.io/forum/#!topic/bokeh/gZHBP8W_TmE
    callback = CustomJS(
        args=dict(source=geosource, date_s = date_slider),
        # Here the update of the geosource
        code="""
        const data = source.data;
        const date = date_s.value;
        
        # Not possible to call a python function here
        data = update_geosource(date)

        source.change.emit();
    """,
    )
    date_slider.on_change('value', callback)

    return date_slider

# Update the geosource taking the date (this is for the slider)
def update_geosource(input_date):
    global geosource
    global gdf

    # Get selected date from source date bezirkeFrame
    bezirkeFrameSelected = bezirkeFrame[bezirkeFrame['TimeLabel'] == str(input_date)]

    if bezirkeFrameSelected.empty:
        return None

    # Merge the GeoDataframe object (gdf) with the Bezirke data (bezirkeFrameSelected)
    merged = pd.merge(gdf, bezirkeFrameSelected, on='iso', how='left')

    # Fill the null values
    values = {'Infected': 0, 'Delta': 0}
    merged = merged.fillna(value=values)

    # Bokeh uses geojson formatting, representing geographical features, with json
    # Convert to json
    merged_json = json.loads(merged.to_json())

    # Convert to json preferred string-like object
    json_data = json.dumps(merged_json)

    geosource = GeoJSONDataSource(geojson=json_data)

    return geosource

def aggregated_by_dates(input_date_0, input_date_1):
    global geosource
    global gdf

    #st.write(input_date_0, input_date_1)

    date_0 = int(input_date_0[0:4]+input_date_0[5:7]+input_date_0[8:10])
    date_1 = int(input_date_1[0:4]+input_date_1[5:7]+input_date_1[8:10])

    #st.write(date_0,date_1)
    
    # Get selected date from source date bezirkeFrame
    results_tidy = pd.DataFrame()

    range_selected = bezirkeFrame.loc[(bezirkeFrame['TimeLabel2'] >= date_0) &
                                        (bezirkeFrame['TimeLabel2'] <= date_1)]

    results_tidy['iso'] = range_selected.groupby('iso')['iso'].max()
    results_tidy['Bezirk'] = range_selected.groupby('iso')['Bezirk'].max()
    results_tidy['Infected'] = range_selected.groupby('iso')['Infected'].max()
    results_tidy['Anzahl_Inzidenz'] = range_selected.groupby('iso')['Anzahl_Inzidenz'].max()
    results_tidy['TimeLabel'] = range_selected.groupby('iso')['TimeLabel'].max()
    results_tidy['TimeLabel2'] = range_selected.groupby('iso')['TimeLabel2'].max()
    results_tidy['Timestamp'] = range_selected.groupby('iso')['Timestamp'].max()
    results_tidy['Delta'] = range_selected.groupby('iso')['Delta'].sum()

    results_tidy.sort_values(by=['TimeLabel'], ascending=False, inplace =True)
    results_tidy.reset_index(drop=True, inplace=True)

    return results_tidy

#@st.cache
def make_map_bezirke(input_column, input_date, palette, input_min_range, input_max_range, option=1, input_date_2=None):
    global geosource
    global gdf

    # Get selected date from source date bezirkeFrame
    if option == 1:
        bezirkeFrameSelected = bezirkeFrame[bezirkeFrame['TimeLabel'] == str(input_date)]
    elif option == 2:
        bezirkeFrameSelected = aggregated_by_dates(input_date,input_date_2)

    if bezirkeFrameSelected.empty:
        return None

    # Merge the GeoDataframe object (gdf) with the Bezirke data (bezirkeFrameSelected)
    merged = pd.merge(gdf, bezirkeFrameSelected, on='iso', how='left')

    # Fill the null values
    values = {'Infected': 0, 'Delta': 0}
    merged = merged.fillna(value=values)

    # Bokeh uses geojson formatting, representing geographical features, with json
    # Convert to json
    merged_json = json.loads(merged.to_json())

    # Convert to json preferred string-like object
    json_data = json.dumps(merged_json)

    geosource = GeoJSONDataSource(geojson=json_data)

    # Add hover tool
    hover = HoverTool(tooltips=[('GKZ', '@iso'),
                                ('County', '@name'),
                                ('Infected', '@Infected'),
                                ('Delta', '@Delta')])

    # Set the format of the colorbar
    min_range = bezirkeFrameSelected[input_column].min()
    max_range = bezirkeFrameSelected[input_column].max()

    if input_max_range != 0:
        max_range = input_max_range

    if input_min_range != 0:
        min_range = input_min_range

    palette = palette

    if input_column == 'Infected':
        palette = brewer['Blues'][8][::-1]

    elif input_column == 'Delta':
        palette = brewer['RdYlGn'][8]

    # Instantiate LinearColorMapper that linearly maps numbers in a range, into a sequence of colors
    color_mapper = LinearColorMapper(palette=palette, low=min_range, high=max_range)

    # Create color bar
    # color_bar = ColorBar(color_mapper=color_mapper, label_standoff=18,
    #                     border_line_color=None, location = (0, 0))
    color_bar = ColorBar(color_mapper=color_mapper)

    # Create figure object
    #perhaps the layout is here

    if option==1:
        p = figure(title=('Austria - Bezirke (' + input_column + ') Date: ' + input_date),
                plot_height=563, plot_width=963, toolbar_location='above')
    elif option==2:
        p = figure(title=('Austria - Bezirke (' + input_column + ') Range of Dates: ' + input_date + " - " + input_date_2),
               plot_height=563, plot_width=963, toolbar_location='above')

    # Specify color bar layout
    ## ADD LAYOUT
    p.add_layout(color_bar, 'right')

    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = None
    p.axis.visible = True

    # Add patch renderer to figure
    p.patches('xs', 'ys', source=geosource, 
            fill_color={'field': input_column, 'transform': color_mapper},
            line_color='black', line_width=0.25, fill_alpha=1)

    # Specify color bar layout
    p.add_layout(color_bar, 'below')

    # Add the hover tool to the graph
    p.add_tools(hover)

    #p.add_layout(children=[[get_slider_dates]], sizing_mode='fixed')

    #if st.checkbox('Write json_data to file json_data.txt'):
    #    f = open('json_data.txt', 'w')
    #    f.write(json_data)
    #    f.close()

    return p

#@st.cache
def download_raw_data_la():
    BASE_URL = ('https://github.com/DataScienceResearchPeru/covid-19_latinoamerica/blob/master/latam_covid_19_data/daily_reports/')
    BASE_URL = ('https://github.com/DataScienceResearchPeru/covid-19_latinoamerica/tree/master/latam_covid_19_data/daily_reports/')
    BASE_URL = ('https://raw.githubusercontent.com/DataScienceResearchPeru/covid-19_latinoamerica/master/latam_covid_19_data/daily_reports/')
    
    range = pd.date_range(start='2/1/2020', end='7/13/2020')
    range_filenames = []
    range_paths = []
    target_dir = 'data/'

    downloaded_filename = target_dir + "downloaded_filenames_LA.json"

    ##open output file for reading
    with open(downloaded_filename, 'r') as filehandle:
        d_list = json.load(filehandle)

    for val in range:
        date_str = "{0:0=2d}".format(val.year) + "{0:0=2d}".format(val.month) + "{0:0=2d}".format(val.day)
        filename = str(val.strftime('%Y-%m-%d')) + '.csv'

        if filename in d_list_co:
            #st.write(filename)
            continue

        url_file = BASE_URL + filename
        
        # fetch URL if exists
        try:
            file_d = urlopen(BASE_URL + filename)
        except urllib.error.URLError as e:
            #st.write(url_file)
            # st.write(e.reason)
            continue

        # Read the file from url

        s = requests.get(url_file).content
        csv_file = pd.read_csv(io.StringIO(s.decode('utf-8')), sep = ',', encoding='utf-8')
        #csv_file = pd.read_csv(file_d, sep = ',', encoding='utf-8')

        # write the file in the adecuate folder
        target_file = target_dir + date_str + '/' + 'Departamentos.csv'
        # target_file = target_dir + date_str + '/' + 'Data_Departamentos_CO.csv'
        csv_file.to_csv(target_file, index=False)

        range_paths.append(target_file)
        range_filenames.append(filename)

    # open output file for writing
    filename_list = target_dir + "downloaded_filenames_LA.json"
    path_list = target_dir + "downloaded_pathnames_LA.json"

    with open(filename_list, 'w') as filehandle:
        json.dump(range_filenames, filehandle)
        #range_filenames.to_csv(target_dir+"Downloaded_filenames_LA.csv", index=False)
    with open(path_list, 'w') as filehandle:
        json.dump(range_paths, filehandle)

    return range_filenames

#@st.cache
def load_departamentos_co():
    global departamentos_co
    departamentos_co.truncate()

    li = []
    counter = 0

    for root, dirs, files in os.walk('data/'):
        for dir in dirs:
            file = 'data/' + dir + '/Departamentos.csv'
            #file = 'data/' + dir + '/Data_Departamentos_CO.csv'


            if os.path.exists(file):
                data = pd.read_csv(file, sep=",", decimal=".",
                                   header=0, dtype={'ISO 3166-2 Code': str})

                data = data[data['Country'] == 'Colombia']
                data['TimeLabel'] = dir[0:4] + '-' + dir[4:6] + '-' + dir[6:8]
                data['TimeLabel2'] = int(dir)
                # calculate daily increase
                if counter > 0:
                    infectedDelta = data['Confirmed'] - infectedCache
                    data['Delta'] = infectedDelta

                li.append(data)
                infectedCache = data['Confirmed']
                counter = counter + 1
    
    departamentos_co = pd.concat(li, axis=0, ignore_index=True)
    departamentos_co = departamentos_co.loc[departamentos_co['Country'] == 'Colombia']
    #departamentos_co[["Deaths", "Recovered", "Confirmed", "Delta"]] = departamentos_co[["Deaths", "Recovered", "Confirmed", "Delta"]].apply(pd.to_numeric)
    #pd.to_numeric(departamentos_co, downcast='integer')
    departamentos_co[["Deaths", "Recovered", "Confirmed", "Delta"]].fillna(0)

    departamentos_co = departamentos_co[departamentos_co['ISO 3166-2 Code'].notna()]
    departamentos_co.rename(columns={'Confirmed': 'Infected'}, inplace=True)
    departamentos_co.rename(columns={'ISO 3166-2 Code': 'iso'}, inplace=True)
    departamentos_co.rename(columns={'Last Update': 'Timestamp'}, inplace=True)

    # write the file in the adecuate folder
    target_file = target_dir + 'Data_Departamentos_CO.csv'
    departamentos_co.to_csv(target_file, index=False)

    return departamentos_co

def aggregated_by_dates_co(input_date_0, input_date_1):

    date_0 = int(input_date_0[0:4]+input_date_0[5:7]+input_date_0[8:10])
    date_1 = int(input_date_1[0:4]+input_date_1[5:7]+input_date_1[8:10])
    #st.write(date_0,date_1)
    
    # Get selected date from source of the departamentos
    results_tidy = pd.DataFrame()

    range_selected = departamentos_co.loc[(departamentos_co['TimeLabel2'] >= date_0) &
                                        (departamentos_co['TimeLabel2'] <= date_1)]

    results_tidy['Subdivision'] = range_selected.groupby('iso')['Subdivision'].max()
    results_tidy['Infected'] = range_selected.groupby('iso')['Infected'].max()
    results_tidy['TimeLabel'] = range_selected.groupby('iso')['TimeLabel'].max()
    results_tidy['TimeLabel2'] = range_selected.groupby('iso')['TimeLabel2'].max()
    results_tidy['iso'] = range_selected.groupby('iso')['iso'].max()
    results_tidy['Recovered'] = range_selected.groupby('iso')['Recovered'].sum()
    results_tidy['Deaths'] = range_selected.groupby('iso')['Deaths'].sum()
    results_tidy['Delta'] = range_selected.groupby('iso')['Delta'].sum()

    return results_tidy

#@st.cache
def make_map_departamentos(input_column, input_date, palette, input_min_range, input_max_range, option=1, input_date_2=None):
    global geosource_co
    global gdf_co

    # Get selected date from source date bezirkeFrame
    if option == 1:
        departamentosFrameSelected = departamentos_co[departamentos_co['TimeLabel'] == str(input_date)]
    elif option == 2:
        departamentosFrameSelected = aggregated_by_dates_co(input_date,input_date_2)

    if departamentosFrameSelected.empty:
        return None

    # Merge the GeoDataframe object (gdf) with the Departamentos data (departamentosFrameSelected)
    merged = pd.merge(gdf_co, departamentosFrameSelected, 
                        how='left', 
                        left_on=['NOMBRE_DPT'], 
                        right_on = ['Subdivision'])

    # Fill the null values
    values = {'Infected': 0, 'Delta': 0}
    merged = merged.fillna(value=values)

    # Bokeh uses geojson formatting, representing geographical features, with json
    # Convert to json
    merged_json = json.loads(merged.to_json())

    # Convert to json preferred string-like object
    json_data = json.dumps(merged_json)

    geosource = GeoJSONDataSource(geojson=json_data)

    # Add hover tool
    hover = HoverTool(tooltips=[('Subdivision', '@NOMBRE_DPT'),
                                ('Infected', '@Infected'),
                                ('Delta', '@Delta'),
                                ('Recovered', '@Recovered'),
                                ('Deaths', '@Deaths')
                                #('COD', '@iso'),
                                ])

    # Set the format of the colorbar
    min_range = departamentosFrameSelected[input_column].min()
    max_range = departamentosFrameSelected[input_column].max()

    if input_max_range != 0:
        max_range = input_max_range

    if input_min_range != 0:
        min_range = input_min_range

    palette = palette

    if input_column == 'Infected':
        palette = brewer['Blues'][8][::-1]

    elif input_column == 'Delta':
        palette = brewer['RdYlGn'][8]


    # Instantiate LinearColorMapper that linearly maps numbers in a range, into a sequence of colors
    color_mapper = LinearColorMapper(palette=palette, low=min_range, high=max_range)

    # Create color bar
    # color_bar = ColorBar(color_mapper=color_mapper, label_standoff=18,
    #                     border_line_color=None, location = (0, 0))
    color_bar = ColorBar(color_mapper=color_mapper)

    # Create figure object
    #perhaps the layout is here

    if option == 1:
        p = figure(title=('Colombia - Departamentos (' + input_column + ') Date: ' + input_date),
                plot_height=736, plot_width=736, toolbar_location='above')
    elif option == 2:
        p = figure(title=('Colombia - Departamentos (' + input_column + ') Range of Dates: ' + input_date + " - " + input_date_2),
               plot_height=736, plot_width=736, toolbar_location='above')

    # Specify color bar layout
    ## ADD LAYOUT
    p.add_layout(color_bar, 'right')

    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = None
    p.axis.visible = True

    # Add patch renderer to figure
    p.patches('xs', 'ys', source=geosource, 
            fill_color={'field': input_column, 'transform': color_mapper},
            line_color='black', line_width=0.25, fill_alpha=1)

    # Specify color bar layout
    p.add_layout(color_bar, 'below')

    # Add the hover tool to the graph
    p.add_tools(hover)

    #p.add_layout(children=[[get_slider_dates]], sizing_mode='fixed')

    #if st.checkbox('Write json_data to file json_data.txt'):
    #    f = open('json_data.txt', 'w')
    #    f.write(json_data)
    #    f.close()

    return p

### MAIN

st.title('C-19 in Austria and Colombia')

if st.checkbox('Update raw data Austria'):
    data_load_state = st.text('Downloading data...')
    data = load_raw_data()
    #st.write(data, len(data))
    #load_bezirke()
    data_load_state.text("Done!")
    st.write("There are " + str(len(data)) + " days recorded")
    #datess = pd.unique(bezirkeFrame['TimeLabel'])
    
    # Other Date Format
    # date_0 = data[0][5:9] +"."+data[0][9:11]+"."+data[0][11:13]
    # date_1 = data[-1][5:9] +"."+data[-1][9:11]+"."+data[-1][11:13]
    
    date_0 = data[0][11:13]+"."+data[0][9:11]+"."+data[0][5:9]
    date_1 = data[-1][11:13]+"."+data[-1][9:11]+"."+data[-1][5:9]
    
    st.write("From " + date_0 + " to " + date_1)

load_bezirke()

if st.checkbox('Update raw data Colombia'):
    data_load_state = st.text('Downloading data...')
    list_data_la = download_raw_data_la()
    #st.write(list_data_la, len(list_data_la))
    data_load_state.text("Done!")
    st.write("There are " + str(len(p_list_co)) + " new days downloaded")
    st.write("There are " + str(len(list_data_la)) + " days up today")
    
    date_0 = list_data_la[0][8:10]+"."+list_data_la[0][5:7]+"."+list_data_la[0][0:4]
    date_1 = list_data_la[-1][8:10]+"."+list_data_la[-1][5:7]+"."+list_data_la[-1][0:4]
    
    st.write("From " + date_0 + " to " + date_1)

load_departamentos_co()

if st.checkbox('Show bezirke-Frame Österreich'):
    st.write(bezirkeFrame, bezirkeFrame.shape)

if st.checkbox('Show departamentos-Frame Colombia'):
    st.write(departamentos_co, departamentos_co.shape)

# Make a slider object
date_today = get_today_date()
myDate = st.date_input(
    'Select date', 
    [date.fromisoformat(date_today),date.fromisoformat(date_today)])
#myDate = st.date_input('Select date', date.fromisoformat('2020-05-21'))
#st.write(myDate)
input_date_1 = myDate[0].isoformat()

if len(myDate) > 1:
    input_date_2 = myDate[1].isoformat()
else:
    input_date_2 = myDate[0].isoformat()

# Define a sequential multi-hue color palette.
palette1 = brewer['Blues'][8][::-1]
palette2 = brewer['RdYlGn'][8]
# Reverse color order so that dark blue is highest obesity.

attribute = st.radio("Please choose attribute to display", ('AUT - Infected', 'AUT - Delta',
                                                            'COL - Infected', 'COL - Delta'))

#st.write(attribute[6:])

#exit()

#Depending on the chosen attribute
if attribute == 'AUT - Infected':
    if input_date_1 == input_date_2:
        # Create the map for a single date
        p1_bezirke = make_map_bezirke(
            input_column=attribute[6:], 
            input_date=input_date_1, 
            palette=palette1, 
            input_min_range=0, 
            input_max_range=0,
            option=1)

    elif input_date_1 != input_date_2:
        # Create the map for a range of dates
        p1_bezirke = make_map_bezirke(
            input_column=attribute[6:], 
            input_date=input_date_1, 
            palette=palette1, 
            input_min_range=0, 
            input_max_range=0,
            option=2,
            input_date_2=input_date_2)

elif attribute == 'AUT - Delta':
    if input_date_1 == input_date_2:
        # Create the map for a single date
        p1_bezirke = make_map_bezirke(
            input_column=attribute[6:], 
            input_date=input_date_1, 
            palette=palette2, 
            input_min_range=-100, 
            input_max_range=100,
            option=1)

    elif input_date_1 != input_date_2:
        # Create the map for a range of dates
        p1_bezirke = make_map_bezirke(
            input_column=attribute[6:], 
            input_date=input_date_1, 
            palette=palette2, 
            input_min_range=-100, 
            input_max_range=100,
            option=2,
            input_date_2=input_date_2)

if attribute == 'COL - Infected':
    if input_date_1 == input_date_2:
        # Create the map for a single date
        p1_bezirke = make_map_departamentos(
            input_column=attribute[6:], 
            input_date=input_date_1, 
            palette=palette1, 
            input_min_range=0, 
            input_max_range=0,
            option=1)

    elif input_date_1 != input_date_2:
        # Create the map for a range of dates
        p1_bezirke = make_map_departamentos(
            input_column=attribute[6:], 
            input_date=input_date_1, 
            palette=palette1, 
            input_min_range=0, 
            input_max_range=0,
            option=2,
            input_date_2=input_date_2)

elif attribute == 'COL - Delta':
    if input_date_1 == input_date_2:
        # Create the map for a single date
        p1_bezirke = make_map_departamentos(
            input_column=attribute[6:], 
            input_date=input_date_1, 
            palette=palette2, 
            input_min_range=-2000, 
            input_max_range=2000,
            option=1)

    elif input_date_1 != input_date_2:
        # Create the map for a range of dates
        p1_bezirke = make_map_departamentos(
            input_column=attribute[6:], 
            input_date=input_date_1, 
            palette=palette2, 
            input_min_range=-2000, 
            input_max_range=2000,
            option=2,
            input_date_2=input_date_2)

if p1_bezirke != None:
    st.bokeh_chart(p1_bezirke)
else:
    st.write('No data available.')

#st.balloons()
#exit()