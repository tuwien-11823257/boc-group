import streamlit as st
import numpy as np
import pandas as pd
import csv

import requests

from zipfile import ZipFile
import zipfile
from urllib.request import urlopen
import urllib.parse

import os
import io
import os.path as path
from os.path import dirname as up

import datetime as date

import plotly.express as px
import matplotlib.pyplot as plt


### MAIN ###

st.title('Interview 2 - BOC-Group - 12.05.2021')
"""
## Data Science Student (20H/W) - Carlos Vargas

In order to accomplish an appropiate Data Analysis Approach, one needs to follow a framework for data management.

For this exercise, the **CRISP-DM framework will be executed**, this consist on the following steps:

**1. Business Understanding**

A good project starts with a deep understanding of the customer’s needs.
The Business Understanding phase focuses on understanding the objectives and requirements of the project. 

** 1.1. Determine business objectives:** You should first “thoroughly understand, from a business perspective, what the customer really wants to accomplish.”

**What do you really want to accomplish?**

**ANSWER HERE:** 
Getting to understand the new Data Science Student and the way to work within the team with the new upcoming member.

** 1.2. Assess situation:** Determine resources availability, project requirements, assess risks and contingencies, and conduct a cost-benefit analysis.

**ANSWER HERE:** 
The resource of time is scare. Therefore, there goal is to come up with the quickest feasible solution to generate _**"good enough"**_ insights and visualizations.

** 1.3. Determine data mining goals:** the technical data mining perspective.

**ANSWER HERE:**

Produce project plan: Step-by-step at implementing CRISP-DM
Select technologies: Streamlit, Python, Plotly (Small web application to deploy fast), git and bitbucket (public repository).

**2. Data Understanding** 

This drives the focus to identify, collect, and analyze the **data sets that can help you accomplish the project goals.**

### This phase also has four tasks:

**2.1. Collect initial data:** Git repository with data of Cardiology or the new data given by the team.

**2.2. Describe data:** (Explanatory analysis below - Section xy)

Examine the data and document its surface properties like data format, number of records, or field identities.

**2.3. Explore data:** (Explanatory analysis below - Section xy)

Dig deeper into the data:

- Query it
- Visualize it
- Identify relationships among the data
- Verify data quality: How clean/dirty is the data?

-- 09:00 - 11:00

**3. Data Preparation:** (Data Preparation below - Section xy)
A common rule of thumb is that **80%** of the project is data preparation.

It has five tasks:

**3.1. Select data:** Determine which data sets will be used and document reasons for inclusion/exclusion.

**3.2. Clean data:** Often this is the lengthiest task. Without it, you’ll likely fall victim to garbage-in, garbage-out. 

A common practice during this task is to **correct, impute, or remove erroneous values.**

-- 11:00 - 13:00

**3.3. Construct data:** Derive new attributes.

**3.4. Integrate data:** Create new data sets by combining data from multiple sources.

**3.5. Format data:** Re-format data as necessary.

-- 13:00 - 15:00

**4. Modeling**
Here you’ll likely build and assess various models based on several different modeling techniques. 

This phase has four tasks:

**4.1. Select modeling techniques:** Determine which algorithms to try (e.g. regression, neural net).


**4.2. Generate test design:** Pending your modeling approach, you might need to split the data into training, test, and validation sets.

**4.3. Build model:** (e.g. “reg = LinearRegression().fit(X, y)”).

**4.4. Assess model:** Generally, multiple models are competing against each other, and the data scientist needs to interpret the model results based on **domain knowledge**, the pre-defined success criteria, and the test design. 

In practice, teams should continue iterating until they find a “good enough” model, proceed through the CRISP-DM lifecycle, then further improve the model in future iterations.


**5. Evaluation**
Whereas the Assess Model task of the Modeling phase focuses on technical model assessment, the Evaluation phase looks more broadly at which model best meets the business and what to do next.

This phase has three tasks:

**5.1. Evaluate results:** Do the models meet the business success criteria? Which one(s) should we approve for the business?

**5.2. Review process:** Review the work accomplished. Was anything overlooked? Were all steps properly executed? Summarize findings and correct anything if needed.

**5.3. Determine next steps:** Based on the previous three tasks, determine whether to proceed to deployment, iterate further, or initiate new projects.

-- 12:00 - 13:00

**6. Deployment**
A model is not particularly useful unless the customer can access its results.
“Depending on the requirements, the deployment phase can be as simple as generating a report or as complex as implementing a repeatable data mining process across the enterprise.”

This final phase has four tasks:

**6.1. Plan deployment:** Develop and document a plan for deploying the model.

**6.2. Plan monitoring and maintenance:** Develop a thorough monitoring and maintenance plan to avoid issues during the operational phase (or post-project phase) of a model.

**6.3. Produce final report:** The project team documents a summary of the project which might include a final presentation of data mining results. **There are standards newly developed to implement principles as **FAIR (findability, accessibility, interoperability, and reusability).**

**6.4. Review project:** Conduct a project retrospective about what went well, what could have been better, and how to improve in the future **BE AGILE**.

– [CRISP-DM Guide](https://www.datascience-pm.com/crisp-dm-2/)

"""


two_up = up(up(__file__))

target_folder = "target/"

df_path = "target/raw.csv"

st.title("Let's get started!")

st.title("2. Data Understanding")

st.header("2.1. Collect initial data")



ds_url_cardio_raw = "https://bitbucket.org/tuwien-11823257/boc-group/raw/e036aae8ca2a7d213a9878e88c69360f5bf62f7a/data-analysis-boc/data/cardiotocography_raw.csv"

ds_url_cardio_zip = "https://bitbucket.org/tuwien-11823257/boc-group/raw/e036aae8ca2a7d213a9878e88c69360f5bf62f7a/data-analysis-boc/data/cardiotocography_raw.zip"

ds_url_input = st.text_area("Please paste the dataset url here:", ds_url_cardio_zip)

separator = st.text_input("Please paste the default separator of the columns in the csv here:", ",")

decimal = st.text_input("Please paste the default decimal symbol of the columns in the csv here:", ".")

header_raw = st.radio("Do you want to skip the header?",("TRUE", "FALSE"))

skip_header = 0

if header_raw == 'TRUE': 
    skip_header = 1
else:
    skip_header = 0
    
#col_names = st.text_area("Please paste column names here:", "b,e,LBE,LB,AC,FM,UC,ASTV,MSTV,ALTV,MLTV,DL,DS,DP,DR,Width,Min,Max,Nmax,Nzeros,Mode,Mean,Median,Variance,Tendency,A,B,C,D,E,AD,DE,LD,FS,SUSP,CLASS,NSP")

#col_names_l = col_names.split(',')


target_folder = st.text_input("Please paste the default target folder to store the datasets:", target_folder)

def download_url(url, save_path, chunk_size=128):
    r = requests.get(url, stream=True)
    with open(save_path, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=chunk_size):
            fd.write(chunk)


def load_raw_data():
    
    url_file = "url.com.co"
    is_zip = 1
    csv_ds_input = pd.DataFrame()
    
    if len(ds_url_input) > 0: url_file = ds_url_input
    
    if url_file[-4:] != ".zip": is_zip = 0
    
    #to test
    #st.write(url_file, is_zip)
    
    if (int(is_zip) == 0):
        csv_ds_input = pd.read_csv(ds_url_input, sep=separator, decimal=decimal, names=col_names_l, skiprows = skip_header, index_col=False)
        
        # to test
        #st.write(csv_ds_input, csv_ds_input.shape)
        path_to_csv = target_folder + "raw.csv"
        csv_ds_input.to_csv(path_to_csv, sep = ";", decimal = ",")
        
        df_path = path_to_csv
            
        
    else:
        try:
            url = urlopen(ds_url_input)
            #st.write(url)
        except urllib.error.URLError as e:
            st.write(e.reason)
    
        time = str(date.datetime.today())
        time_ = time.replace(" ", "_")
        time_ = time_.replace(":", ".")
        
        #to test
        #st.write(time_)
        
        file_name = str(time_)+"_raw_csv.zip"

        zip_folder = target_folder 
                
        r = requests.get(ds_url_input)
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall(zip_folder)
        
        path = zip_folder + "cardiotocography_raw.csv"
        
        csv_ds_input = pd.read_csv(path, sep=separator, decimal=decimal, names=col_names_l, skiprows = skip_header, index_col=False)
        
        # to test
        #st.write(csv_ds_input, csv_ds_input.shape)
        path_to_csv = target_folder + "raw.csv"
        csv_ds_input.to_csv(path_to_csv, sep = ";", decimal = ",")
        df_path = path_to_csv
        
        
        

# Call the function
#load_raw_data()

# to test

st.header("Data Understanding")

st.subheader("2.1. Collect initial data")

with st.echo():
    df_path = "target/cardiotocography_raw.csv"
    csv_ds_input = pd.read_csv(df_path, sep=",", decimal=".",index_col=False)
    
    col_names_2 = st.text_area("Please paste column names here:", "b;e;LBE;LB;AC;FM;UC;ASTV;MSTV;ALTV;MLTV;DL;DS;DP;DR;Width;Min;Max;Nmax;Nzeros;Mode;Mean;Median;Variance;Tendency;A;B;C;D;E;AD;DE;LD;FS;SUSP;CLASS")

    col_names_l_2 = col_names_2.split(';')
    st.write(len(col_names_l_2))

st.subheader("2.2. Describe data")

with st.echo():
    df_cardio = csv_ds_input.iloc[:,:] # Rows , Columns
    df_cardio.columns = col_names_l_2
    #st.write(df_cardio.types)
    st.write(df_cardio.describe())
    st.write(df_cardio, df_cardio.shape)
    
with st.echo():
    
    dfg = df_cardio.groupby(["CLASS"]).count()
    dfg["Count"] = dfg.iloc[:,1]
    #st.write(dfg)
    fig = px.bar(dfg, x=dfg.index, y="Count")
    st.plotly_chart(fig)
    #fig.show()

#df_cardio.groupby(["CLASS"]).count().reset_index().plot(x='Class', y='value', kind='bar')

st.subheader("2.3. Explore data")

"""
Dig deeper into the data:

- Query it  - Done
- Visualize it - Done
- Identify relationships among the data - Progress
- Verify data quality: How clean/dirty is the data? - 

"""


# Violin chart with distribution per column
def display_graph(marginal):
    df = df_cardio
    fig = px.histogram(df, x="total_bill", y="tip", color="CLASS",
                    marginal="box", # or violin, rug
                    hover_data=df.columns)
    #fig.show()
    return fig


#You can make subplots using plotly's make_subplots() function. From there you add traces with the desired data and position within the subplot.

from plotly.subplots import make_subplots
import plotly.graph_objects as go


fig = make_subplots(rows=3, cols=3)

fig.add_trace(
    go.Histogram(x=df_cardio['b'], name="b"), 
    row=1, col=1#, text='b'
)
fig.add_trace(
    go.Histogram(x=df_cardio['e'], name="e"),
    row=1, col=2
)
fig.add_trace(
    go.Histogram(x=df_cardio['LBE'], name="LBE"),
    row=1, col=3
)
fig.add_trace(
    go.Histogram(x=df_cardio['LB'], name="LB"),
    row=2, col=1
)
fig.add_trace(
    go.Histogram(x=df_cardio['AC'], name="AC"),
    row=2, col=2
)
fig.add_trace(
    go.Histogram(x=df_cardio['FM'], name="FM"),
    row=2, col=3
)
fig.add_trace(
    go.Histogram(x=df_cardio['UC'], name="UC"),
    row=3, col=1
)
fig.add_trace(
    go.Histogram(x=df_cardio['MSTV'], name="MSTV"),
    row=3, col=2
)
fig.add_trace(
    go.Histogram(x=df_cardio['CLASS'], name="CLASS"),
    row=3, col=3
)

with st.echo():
    st.plotly_chart(fig)
    
st.subheader("Relationships among the data")
#import plotly.plotly as py
#from plotly.offline import iplot
corr = df_cardio.corr()

heat = go.Heatmap(z=corr.values,
                x=corr.index.values,
                y=corr.columns.values)

title = 'Cardiology Correlation Matrix'

layout = go.Layout(
    title_text=title, 
    title_x=0.5, 
    width=600, 
    height=600,
    xaxis_showgrid=False,
    yaxis_showgrid=False,
    yaxis_autorange='reversed'
)
fig=go.Figure(data=[heat], layout=layout)

with st.echo():
    st.plotly_chart(fig)  
    
    
st.header("3. Data Preparation")

#df["a*b"] = df["a"] * df["b"]
    
st.subheader("Subsampling based on conditions")

with st.echo():
    cd_cardio_class_1 = df_cardio[df_cardio['CLASS'] == 1]
    cd_cardio_class_2 = df_cardio[df_cardio['CLASS'] == 2]
    
    st.write(cd_cardio_class_1, cd_cardio_class_1.shape)
    st.write(cd_cardio_class_2, cd_cardio_class_2.shape)
    
with st.echo():    
    # Create the conditions as a list
    conditions = [
    (df_cardio['LB'] <= 136),
    (df_cardio['LB'] >= 136) & (df_cardio['Nzeros'] >= 2),
    (df_cardio['LB'] >= 136) & (df_cardio['Tendency'] >= 1),
    (df_cardio['LB'] >= 169)
    ]
    
    # Create a list of the values to assign for each condition
    values = ['Mild', 'Moderate', 'High', 'Very High']
    
    # Create a new column based on the conditions and the values
    df_cardio['Heart_attack_risk'] = np.select(conditions, values)
    
    st.write(df_cardio, df_cardio.shape)
    
    
st.subheader("Missing Values")

# Lets say that missing values in this dataset are the zeros in Heart_attack_risk
from sklearn.impute import SimpleImputer
df_cardio['Heart_attack_risk'].mask(df_cardio['Heart_attack_risk'] == '0', np.nan, inplace=True)
st.write(df_cardio, df_cardio.shape)

with st.echo():
    df_cardio_imp = df_cardio
    
    #st.write(df_cardio.isnull().sum())
    st.write(df_cardio.isna().sum())
    
    imp = SimpleImputer(missing_values=np.nan, strategy='constant', fill_value = "zero risk")
    imp.fit(df_cardio_imp)
    #SimpleImputer()
    st.write(imp.transform(df_cardio_imp))
    

   
    
    

        


"""
## Technical Path
## 0. Categorical vs. Nominal
### 0.1. Data Transformation - Done!
### 0.2. NaN Imputations - Done!
### 0.3. Data Aggreagation - Done!
## 1. Distributions with Boxplots - Done!
## 2. Anova and Normality tests - Almost.. ok!
## 3. Correlations between variables - Done!

## 4. Importance of Variables:
### 4.1. Principal Components Analysis (PCA)
### 4.1. F-Test (F-Importance)
### 4.2. 
## 5. Cluster Analysis
### 5.1. Hierarquical Clustering (K-Means)
### 5.2. K-means with centroids
### 5.3. 
## 6. Machine Learning
### 6.1. Split Subsampling Training and Testing
### 6.2. Model Selection, tuning and evaluation
### 6.2. Cross Validation
### 6.3. 
"""


st.write('No data available.')

st.balloons()
#exit()